package ru.testing;

import com.mashape.unirest.http.exceptions.UnirestException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.testing.entities.Translations;
import ru.testing.gateway.YandexTranslateGateway;

import java.util.List;

public class YandexTranslateTest
{
    @Test
    public void helloWorldTest() throws UnirestException
    {
        List<String> texts = List.of("Hello world!", "Hello");
        YandexTranslateGateway gateway = new YandexTranslateGateway();
        Translations expected = gateway.getTranslation(texts, "en", "ru");
        Assertions.assertEquals("������, ���!", expected.getTexts().get(0).getText());
    }

    @Test
    public void helloWorldTest1() throws UnirestException
    {
        String text = "Hello world!";
        YandexTranslateGateway gateway = new YandexTranslateGateway();
        Translations expected = gateway.getTranslation(text, "en", "ru");
        Assertions.assertEquals("������, ���!", expected.getTexts().get(0).getText());
    }
}
