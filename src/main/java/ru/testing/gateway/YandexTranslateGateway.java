package ru.testing.gateway;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.extern.slf4j.Slf4j;
import ru.testing.entities.Translations;

import java.util.List;

@Slf4j
public class YandexTranslateGateway {
    private static final String URL = "https://translate.api.cloud.yandex.net/translate/v2/translate";
    private static final String TOKEN = "";
    private static final String FOLDER_ID = "";

    public Translations getTranslation(String sourceText, String sourceLanguage, String targetLanguage) throws UnirestException
    {
        Gson gson = new Gson();
        String body = createBody(sourceText, sourceLanguage, targetLanguage);
        HttpResponse<String> response = Unirest.post(URL)
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + TOKEN)
                .body(body)
                .asString();
        String strResponse = response.getBody();
        return gson.fromJson(strResponse, Translations.class);
    }

    public Translations getTranslation(List<String> sourceText, String sourceLanguage, String targetLanguage) throws UnirestException
    {
        String strSourceText = String.join("\",\"", sourceText);
        return this.getTranslation(strSourceText, sourceLanguage, targetLanguage);
    }

    private String createBody(String sourceText, String sourceLanguage, String targetLanguage)
    {
        String body = "{\"texts\": [\"" + sourceText + "\"],"
                + "\"sourceLanguageCode\": \"" + sourceLanguage + "\","
                + "\"targetLanguageCode\": \"" +  targetLanguage + "\","
                + "\"folderId\": \"" + FOLDER_ID + "\"}";
        return body;
    }
}
