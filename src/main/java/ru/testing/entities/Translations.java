package ru.testing.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Translations
{
    @SerializedName("translations")
    private List<Translation> texts;

    public Translations(List<Translation> texts) {
        this.texts = texts;
    }

    public List<Translation> getTexts() {
        return texts;
    }

    public void setTexts(List<Translation> texts) {
        this.texts = texts;
    }
}
