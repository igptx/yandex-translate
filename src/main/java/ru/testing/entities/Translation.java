package ru.testing.entities;

import com.google.gson.annotations.SerializedName;

public class Translation
{
    @SerializedName("text")
    private String text;

    public Translation(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
